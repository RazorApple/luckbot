const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "prestige",
    category: "core",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id)
        if(user.prestige !== 4) {
          if(user.luck == (user.prestige*100)) {
            message.channel.send(`Prestiged!
                Luck reset to 1,
                Max amount of possible luck incresed to: ${((user.prestige+0.5)*100)}
                from: ${(user.prestige*100)}
            `)

            user.prestige += 0.5
            user.luck = 1;

            client.accounts.set(message.author.id, user)
          } else {
            message.channel.send(`${config.error} Your luck isn't ${(user.prestige*100)}!`)
          }
      } else {
        message.channel.send(`${config.error} Prestiges maxed!`)
      }
    }
};
