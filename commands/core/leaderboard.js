const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "leaderboard",
    category: "core",
    run: async (args, client, message) => {
        let ranking = 0;

        const top10 = client.accounts
            .array()
            .sort((a, b) => b.luck - a.luck)
            .splice(0, 10);

        const embed = new Discord.MessageEmbed()
            .setColor(config.color)
            .setTitle(`Leaderboard`);

        for (const data of top10) {
            const user = await client.users.fetch(data.id);

            ranking += 1;
            embed.addField(
                `#${ranking} | ${user.tag}`,
                `${data.luck}/${data.prestige*100} ${config.emoji}`
            );
        }

        return message.channel.send(embed);
    }
};
