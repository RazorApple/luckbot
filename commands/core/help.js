const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "help",
    category: "core",
    run: (args, client, message) => {
        let categories = [];

        client.commands.forEach(command => {
            let category = client.filter(categories, a => {
                return a.category === command.category;
            });
            if (category.length == 0) {
                categories.push({
                    category: command.category,
                    commands: [command]
                });
            } else {
                for (var i = 0; i < categories.length; i++) {
                    if (categories[i].category == command.category)
                        categories[i].commands.push(command);
                }
            }
        });

        if (!args[0]) {
            const embed = new Discord.MessageEmbed()
                .setColor(config.color)
                .setTitle("Help")
                .setDescription(
                    `How to use the help system:
        \`${config.prefix}help category\`
        `
                )
                .addField("\u200b", "\u200b")
                .addField(
                    "Available categories:",
                    categories.map(a => a.category).join(", ")
                );

            return message.channel.send(embed);
        }

        args[0] = args[0].toLowerCase();

        if (
            categories.some(a => {
                return a.category === args[0];
            })
        ) {
            let category = client.filter(categories, a => {
                return a.category == args[0];
            })[0];

            let commands = [];

            category.commands.forEach(a => {
                commands.push(a.name);
            });

            message.channel.send(`Commands of ${args[0]}: ${commands.join(", ")} `);
        } else {
            message.channel.send(`${config.error} Category does not exist.`);
        }
    }
};
