const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "profile",
    category: "core",
    run: async (args, client, message) => {
        let accountUser;
        let user;

        if (args[0]) {
            if (client.accounts.get(args[0])) {
                user = client.accounts.get(args[0])
                if (await client.users.fetch(args[0])) {
                    accountUser = await client.users.fetch(args[0]);
                }
            } else {
                let targetMember = message.mentions.users.first();
                if (!targetMember) return message.channel.send(`${config.error} No user ID or mention found.`)
                user = client.accounts.get(targetMember.id)
                accountUser = targetMember
            }
        } else {
            accountUser = message.author
            user = client.accounts.get(accountUser.id)
        }

        if (!accountUser) {
            return message.channel.send(`${config.error} Cannot find user in cache.`)
        }

        if (!user) {
            return message.channel.send(`${config.error} User has never talked.`)
        }

        const embed = new Discord.MessageEmbed()
            .setColor(config.color)
            .setTitle(`Profile of ${accountUser.username}`)
            .addField("Luck:", `${user.luck}/${user.prestige*100} ${config.emoji}`, true)

        message.channel.send(embed)
    }
};
