const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "invite",
    category: "core",
    run: (args, client, message) => {
        message.author.send("Bot Invite: https://discord.com/api/oauth2/authorize?client_id=745213389352992778&permissions=0&scope=bot");
        return message.channel.send(":mailbox: You've got mail!");
    }
};
