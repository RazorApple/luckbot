const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "info",
    category: "core",
    run: (args, client, message) => {
        let seconds = (client.uptime / 1000);

        let time = {
          days: Math.floor(seconds / (3600*24)),
          hours: Math.floor(seconds % (3600*24) / 3600),
          minutes: Math.floor(seconds % 3600 / 60),
          seconds: Math.floor(seconds % 60),
          ms: Date.now() - message.createdTimestamp
        }

        const embed = new Discord.MessageEmbed()
            .setColor(config.color)
            .setTitle(`Info`)
            .addField(":page_facing_up: Uptime", `${time.days}d ${time.hours}h ${time.minutes}m ${time.seconds}s`, true)
            .addField(":ping_pong:  Latency", time.ms, true)
            .addField(":evergreen_tree: DiscordJS version", require("../../package.json").dependencies["discord.js"].substring(1), true)
            .addField(":person_doing_cartwheel: Players", client.accounts.array().length , true)
            .addField(":person_fencing: Guilds", client.guilds.cache.size, true)
            .addField(":wine_glass: Creator", "https://dsc.bio/gom", true)
            
        return message.channel.send(embed);
    }
};
