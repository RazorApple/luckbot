const Discord = require("discord.js");
const config = require("../../config");

function convertMS(ms) {
    var d, h, m, s;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;
    return {
        d: d,
        h: h,
        m: m,
        s: s
    };
};

module.exports = {
    name: "reroll",
    category: "core",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id);

        let correct = () => {
            user.rolltime = Date.now()
            user.luck = Math.floor(Math.random() * (user.prestige * 100)) + 1;
            client.accounts.set(message.author.id, user);
            return message.channel.send(`Luck re-rolled! Check ${config.prefix}profile!`)
        }

        if (user.rolltime) {
            if (Date.now() < user.rolltime + 86400000) {
                var dif = convertMS(Math.round(((user.rolltime + 86400000) - Date.now())));
                return message.channel.send(`${config.error} You're already rerolled a while ago. Remaining Time: ${dif.d}d ${dif.h}h ${dif.m}m ${dif.s}s`)
            } else {
                return correct();
            }
        } else {
            return correct();
        }
    }
};
