const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "joinguild",
    category: "guild",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id);

        if (!args[0]) {
            return message.channel.send(
                `How to use this: ${config.prefix}joinguild guildname`
            );
        }

        args[0] = args[0].toLowerCase();

        if (!user.guild) {
            let guild = client.guild.get(args[0]);

            if (guild) {
                if (guild.open) {
                    user.guild = args[0];
                    guild.members.push(message.author.id);
                    client.accounts.set(message.author.id, user);
                    client.guild.set(args[0], guild);
                    message.channel.send(`You're now in ${args[0]}! Yay!`);
                } else {
                    message.channel.send(`${config.error} This guild isn't open!`);
                }
            } else {
                message.channel.send(`${config.error} This guild doesn't exist!`);
            }
        } else {
            message.channel.send(
                `${config.error} You are already in a guild! Leave with ${config.prefix}leaveguild!`
            );
        }
    }
};
