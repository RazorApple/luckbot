const Discord = require("discord.js");
const config = require("../../config");
module.exports = {
    name: "infoguild",
    category: "guild",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id)

        const sucess = async (guildid) => {
          let guild = client.guild.get(guildid);
          let members = [];
          let totalLuck = 0;
          let totalPrestige = 0;

          if(guild) {

            guild.members.forEach(async (member) => {
                let memberUser = client.accounts.get(member)
                members.push((await client.users.fetch(member)).tag)
                totalLuck += memberUser.luck * 1
                totalPrestige += memberUser.prestige * 100
            })


            const embed = new Discord.MessageEmbed()
                .setColor(config.color)
                .setTitle(`Guildinfo of ${guildid}`)
                .addField("Owner:", (await client.users.fetch(guild.owner)).tag)
                .addField("Members:", members.join(", "))
                .addField("Total luck:", `${totalLuck}/${totalPrestige} ${config.emoji}`);

            message.channel.send(embed)

          } else {
            message.channel.send(`${config.error} This guild doesn't exist!`)
          }
        }

        if(args[0]) {
          sucess(args[0])
        } else {
          if(user.guild) {
            sucess(user.guild)
          } else {
            message.channel.send(`${config.error} You aren't in a guild! Provide a guild!`);
          }
        }
    }
};
