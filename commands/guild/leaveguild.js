const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "leaveguild",
    category: "guild",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id);

        if (user.guild) {
            let guild = client.guild.get(user.guild)
            if (guild) {
                if (guild.owner !== message.author.id) {
                    guild.members = guild.members.filter(item => item !== message.author.id)
                    let oldUserGuild = user.guild
                    user.guild = undefined

                    client.accounts.set(message.author.id, user)
                    client.guild.set(oldUserGuild, guild)
                    message.channel.send(`You've left ${oldUserGuild}!`)
                } else {
                    message.channel.send(`${config.error} You're a owner! Use ${config.prefix}deleteguild instead!`)
                }
            } else {
                console.log(`${config.error} Malformed guild`)
            }
        } else {
            message.channel.send(`${config.error} You are not in a guild!`)
        }
    }
};
