const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "makeguild",
    category: "guild",
    run: (args, client, message) => {
        let temp = client.accounts.get(message.author.id);

        if (!args[0]) {
            return message.channel.send(
                `How to use this: ${config.prefix}mkguild guildname`
            );
        }

        args[0] = args[0].toLowerCase();

        if (!(args[0].length > 3)) {
            if (!client.guild.get(args[0])) {
                if (!temp.guild) {
                    client.guild.set(args[0], {
                        owner: message.author.id,
                        members: [message.author.id],
                        open: false
                    });
                    temp.guild = args[0];
                    client.accounts.set(message.author.id, temp);
                    message.channel.send("Guild made!");
                } else {
                    message.channel.send(`${config.error} You are already in a guild! Leave with luck!leaveguild!`)
                }
            } else {
                message.channel.send(`${config.error} Guild name already taken`);
            }
        } else {
            return message.channel.send(
                `${config.error} Guild name cannot be longer than 3 letters.`
            );
        }
    }
};
