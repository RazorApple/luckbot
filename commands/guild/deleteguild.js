const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "deleteguild",
    category: "guild",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id);

        if (user.guild) {
            let guild = client.guild.get(user.guild)
            if (guild) {
                if (guild.owner === message.author.id) {

                    guild.members.forEach((a) => {
                        let acc = client.accounts.get(a)
                        acc.guild = undefined
                        client.accounts.set(a, acc)
                    })

                    client.guild.set(user.guild, undefined)

                    message.channel.send(`Guild ${user.guild} deleted!`)

                } else {
                    message.channel.send(`${config.error} You do not own this guild`)
                }
            } else {
                console.log(`${config.error} Malformed guild`)
            }
        } else {
            message.channel.send(`${config.error} You are not in a guild!`)
        }
    }
};
