const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "openguild",
    category: "guild",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id);

        if (user.guild) {
            let guild = client.guild.get(user.guild);
            if (guild.owner === message.author.id) {
                if (!guild.open) {
                    guild.open = true;
                    client.guild.set(user.guild, guild);
                    return message.channel.send("Guild opened! People can now join!");
                } else {
                    return message.channel.send(`${config.error} Guild was already open!`);
                }
            } else {
                message.channel.send(`${config.error} You are NOT the owner of this guild!`);
            }
        } else {
            message.channel.send(`${config.error} You are not in a guild.`);
        }
    }
};
