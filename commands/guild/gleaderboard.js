const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "gleaderboard",
    category: "guild",
    run: (args, client, message) => {
        let ranking = 0;
        let top10 = [];

        client.accounts.array().forEach(account => {
            if (account.guild) {
                let guild = client.guild.get(account.guild);
                let totalLuck = 0;
                let maxLuck = 0;

                guild.members.forEach(a => {
                    let temp = client.accounts.get(a);
                    totalLuck += temp.luck * 1;
                    maxLuck += temp.prestige*100;
                });

                if (!top10.some((a) => {
                        return a.name === account.guild
                    }))
                    top10.push({
                        name: account.guild,
                        luck: totalLuck,
                        prestige: maxLuck
                    });
            }
        });
        top10 = top10
            .sort((a, b) => b.luck - a.luck)
            .splice(0, 10);

        const embed = new Discord.MessageEmbed()
            .setColor(config.color)
            .setTitle(`Guild Leaderboard`);

        for (const data of top10) {
            ranking += 1
            embed.addField(
                `#${ranking} | ${data.name}`,
                `${data.luck}/${data.prestige} ${config.emoji}`
            );
        }

        return message.channel.send(embed);
    }
};
