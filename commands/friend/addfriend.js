const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "addfriend",
    category: "friend",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id);
        let firstUser = message.mentions.users.first();

        if (firstUser) {
            let firstAccount = client.accounts.get(firstUser.id);
            if (firstAccount) {
                if(firstUser !== message.author) {
                  if(!user.friends.includes(firstUser.id)) {
                    user.friends.push(firstUser.id);
                    client.accounts.set(user.id, user);
                    message.channel.send(`You've added ${firstUser.tag} as your friend!`);
                  } else {
                    message.channel.send(`${config.error} Already friended.`)
                  }
                } else {
                  message.channel.send(`${config.error} You can't friend yourself. );`)
                }
            } else {
                message.channel.send(`${config.error} User has never talked.`);
            }
        } else {
            message.channel.send(`${config.error} Please mention a person!`);
        }
    }
};
