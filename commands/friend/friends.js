const Discord = require("discord.js");
const config = require("../../config");

function capitalize(string) {
    return string[0].toUpperCase() + string.slice(1);
}

function callback(mutuals, nonMutuals, embed, message) {
  if (!(mutuals.length === 0)) {
      embed.addField("Mutuals:", mutuals.join(", "));
  }

  if (!(nonMutuals.length === 0)) {
      embed.addField("Non-Mutuals:", nonMutuals.join(", "));
  }

  message.channel.send(embed)
}

module.exports = {
    name: "friends",
    category: "friend",
    run: async (args, client, message) => {

        let user = client.accounts.get(message.author.id);
        let first = message.mentions.users.first();
        const embed = new Discord.MessageEmbed()
            .setColor(config.color)
            .setTitle(`Friends of ${message.author.username}`)


        if (user.friends.length === 0) {
            message.channel.send("Awsh. 0 friends. Maybe go out and make some?")
        } else {
             for (var i = 0; user.friends.length>i; i++) {
              let mutuals = [];
              let nonMutuals = [];

              let friend = client.accounts.get(user.friends[i]);
              let friendUser = await client.users.fetch(user.friends[i]);

              if (!friend.friends.includes(user.id)) {
                  nonMutuals.push(`${friendUser.tag}`);
              } else {
                  mutuals.push(`${friendUser.tag} (${capitalize(friendUser.presence.status)})`);
              }

              if(user.friends.length-1 == i) {
                callback(mutuals, nonMutuals, embed, message)
              }
            }
        }

    }
};
