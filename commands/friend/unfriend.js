const Discord = require("discord.js");
const config = require("../../config");

module.exports = {
    name: "unfriend",
    category: "friend",
    run: (args, client, message) => {
        let user = client.accounts.get(message.author.id);
        let first = message.mentions.users.first();

        if (first) {
            let mentionedUser = client.accounts.get(first.id);
            if (mentionedUser) {
                if (first !== message.author) {
                    if (user.friends.includes(first.id)) {
                        user.friends = client.filter(user.friends, item => item !== first.id)
                        client.accounts.set(message.author.id, user)
                        message.channel.send(`You've unfriended ${first.tag}!`)
                    } else {
                        return message.channel.send(`${config.error} You're not friends with this person!`)
                    }
                } else {
                    return message.channel.send(`${config.error} You cannot unfriend yourself!`)
                }
            } else {
                return message.channel.send(`${config.error} This user has not talked or is a bot.`)
            }
        } else {
            return message.channel.send(`${config.error} Please mention a person!`)
        }
    }
};
