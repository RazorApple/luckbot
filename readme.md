# Lucky
## Description
Lucky is a discord bot that works by using a random number generator.

Once you talk in a discord server with Lucky invited, you'll be given a random number. This number is called Luck. It's completely random number,
and that's why it's Luck. Luck will then determine your rankings in the Leaderboards.

You can see your Luck with luck!profile.

### Re-rolling
Re-rolling can is done by using the command "luck!reroll". This command will reroll your amount of Luck. You can only run this command once a day.

### Guilds
Guilds are a sizable part of Lucky. You can make a guild and invite your friends. The more people you have with higher luck amounts, the better total luck you have on
the Guild Leaderboards.
You can see guild leaderboards with luck!gleaderboard.

### Prestige
Prestiging is like, starting everything over, but with more power.
Once you prestige, your luck will be nullified. But, once you reroll after Prestiging, your max luck will be incresed. There's a limit on 6 prestiges. Every prestige your max luck will go up by 50.

## Discord
[Click here!](https://discord.gg/jHBVCag)

## Contact me
[Portfolio](https://lukewarmcat.codes) | [Discord Bio](https://dsc.bio/gom)
