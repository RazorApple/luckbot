const Discord = require("discord.js");
const client = new Discord.Client({
    disableEveryone: true
});
const Enmap = require("enmap");
const fs = require('fs')
const path = require('path')

const config = require("./config")

const readdirSync = (p, a = []) => {
    if (fs.statSync(p).isDirectory())
        fs.readdirSync(p).map(f => readdirSync(a[a.push(path.join(p, f)) - 1], a))
    return a
}

const fsCommands = readdirSync(__dirname + "/commands");

client.accounts = new Enmap({
    name: "accounts"
});
client.guild = new Enmap({
    name: "guild"
});

client.filter = (array, fn) => {
    var results = [];
    var item;
    for (var i = 0, len = array.length; i < len; i++) {
        item = array[i];
        if (fn(item)) results.push(item);
    }
    return results;
};


client.commands = [];

fsCommands.forEach(file => {
    if (path.extname(file) == ".js") {
        let command = require(file);

        client.commands.push(command);
    }
});

client.on("ready", () => {
    console.log(
        `LuckyBot loaded`
    );
    client.user.setPresence({
        activity: {
            name: `${config.prefix}help | v${config.version}`
        }
    });
});

client.on("message", message => {
    if (message.author.bot) return;
    let user = client.accounts.get(message.author.id);

    if (!user) {
        client.accounts.set(message.author.id, {
            id: message.author.id,
            luck: Math.floor(Math.random() * 100) + 1,
            friends: [],
            prestige: 1
        });
    }

    if(Math.floor(Math.random() * 1000) == 999) {
      let randomArray = ["prestige", "luck"]
      let random = Math.floor(Math.random() * randomArray.length);

      switch (randomArray[random]) {
        case "prestige":

          if(user.prestige !== 4) {
            user.prestige += 0.5
            client.accounts.set(message.author.id, user)
            message.channel.send("You've found a 5 Leaf Clover (0.1%). You got a prestige levelup!")
          } else {
            message.channel.send("Awsh dude. Since you already have maxed prestige, you probably don't need this.");
          }

          break;

        case "luck":

          let end = 0;

          if((user.luck+50) > user.prestige*100) {
            end = user.prestige*100
          } else {
            end = user.luck+50
          }

          if(user.luck !== user.prestige*100) {
            user.luck = end
            client.accounts.set(message.author.id, user)
            message.channel.send(`You've found a 5 Leaf Clover (0.1%). You got 50 luck! (Overflows of ${user.prestige*100} are removed)`)
          } else {
            message.channel.send("Awsh dude. Since you already have maxed luck, you probably don't need this.")
          }
    }
  }

    if (message.content.startsWith(config.prefix)) {
        const args = message.content.split(" ");
        const command = args
            .shift()
            .substr(config.prefix.length)
            .toLowerCase();

        client.commands.forEach((com) => {
            if (command === com.name) {
                com.run(args, client, message)
            }
        })
    }
});

client.login(config.token);
